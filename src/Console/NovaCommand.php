<?php

namespace SC\Setting\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class NovaCommand extends Command
{
    protected $signature = 'setting:nova';
    protected $description = 'add nova resource for Setting';

    public function handle(FileSystem $fs)
    {
        $src = realpath(__DIR__ . '/../../resources/stubs/Setting.php.stub');
        $dest = app_path('Nova/Setting.php');
        if ($fs->exists($dest)) {
            $this->error('file already exists: ' . $dest);
        } else {
            if ($fs->copy($src, $dest)) {
                $this->info($src . ' => ' . $dest);
            } else {
                $this->error('copy file error. from: ' . $src . ', to: ' . $dest . '.');
            }
        }
    }
}