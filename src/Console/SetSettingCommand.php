<?php

namespace SC\Setting\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class SetSettingCommand extends Command
{
    protected $signature = 'setting:set {key} {value}';
    protected $description = 'set Setting value';

    public function handle()
    {
        setting([
            $this->argument('key') => $this->argument('value'),
        ], true);
        $this->info('Update value success!');
    }
}