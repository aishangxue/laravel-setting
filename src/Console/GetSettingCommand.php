<?php

namespace SC\Setting\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class GetSettingCommand extends Command
{
    protected $signature = 'setting:get {key}';
    protected $description = 'get Setting value';

    public function handle()
    {
        $key = $this->argument('key');
        $value = setting($key) ?: '-';
        $this->info(sprintf("Setting value of \"%s\" is: %s", $key, $value));
    }
}