<?php

namespace SC\Setting;

use Illuminate\Support\ServiceProvider;


class SettingServiceProvider extends ServiceProvider
{
    protected $commands = [
        Console\GetSettingCommand::class,
        Console\SetSettingCommand::class,
        Console\NovaCommand::class,
    ];

    public function boot()
    {
        $this->loadMigrationsFrom(realpath(__DIR__ . '/../migrations'));
    }

    public function register()
    {
        if ($this->app->runningInConsole()) {
            $this->commands($this->commands);
        }
    }
}